import { DarkSkySearchService } from './services/darksky.service';
import { SearchComponent } from './search-component/search.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherComponent } from './weather-app.component';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { WeatherItemComponent } from './weather-item/weather-item.component';
import { TempConverterPipe } from './pipes/to-celsius.pipe';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {
  MatFormFieldModule,
  MatInputModule,
} from '@angular/material';


const routes: Routes = [
  { path: 'weather', component: WeatherComponent }
];

@NgModule({
  declarations: [
    WeatherComponent,
    SearchComponent,
    WeatherItemComponent,
    TempConverterPipe
  ],
  imports: [
    RouterModule.forChild(routes),
    BrowserModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    HttpClientJsonpModule
  ],
  exports: [],
  providers: [DarkSkySearchService]
})

export class WeatherModule {}
