import { IWeather } from './../Interfaces/IWeather';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DarkSkySearchService } from '../services/darksky.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-weather-item',
  templateUrl: './weather-item.component.html',
  styleUrls: ['./weather-item.component.scss']
})

export class WeatherItemComponent implements OnInit, OnDestroy {

  constructor(private _darkSky: DarkSkySearchService) { }

  public currentDate = new Date();
  public weather: IWeather;
  public cityName: string = null;
  public summary: string;
  public iconType: string;
  public visibility: number;
  public windSpeed: number;
  public temperature: number;
  public cloudCover: number;
  weatherSubscription: Subscription;

  ngOnInit() {
    this.weatherSubscription = this._darkSky.weatherSummary.subscribe((weather: IWeather) => {
      const { summary: summary, visibility: visibility, windSpeed: windSpeed, temperature: temperature,
              cloudCover: cloudCover, icon: iconType } = weather.currently;

      this.cityName = this._darkSky.cityName;
      this.weather = weather;
      this.summary = summary;
      this.visibility = visibility;
      this.windSpeed = windSpeed;
      this.temperature = temperature;
      this.cloudCover = cloudCover;
      this.iconType = iconType;
    });

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this._darkSky.getWeather(position.coords.latitude, position.coords.longitude);
      });
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }

  ngOnDestroy() {
    this.weatherSubscription.unsubscribe();
  }
}
