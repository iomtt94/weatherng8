import { Component, OnInit, OnDestroy } from '@angular/core';
import { DarkSkySearchService } from '../services/darksky.service';
import { ICity } from '../Interfaces/ICity';
import { debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit, OnDestroy {

  constructor(private _darkSky: DarkSkySearchService) {}

  public userSearchText = '';
  public citiesList: any[];
  public isValidSearch = false;
  public citiesListSubscription: Subscription;

  ngOnInit() {}

  searchCityValidation(): void {
    if (this.userSearchText.length < 2) {
      this.isValidSearch = false;
      return;
    }

    this.citiesListSubscription = this._darkSky.searchCity(this.userSearchText)
      .pipe(debounceTime(200))
      .subscribe((foundCities: ICity[]) => {
        this.citiesList = foundCities['RESULTS'].filter((city: ICity) => city.type === 'city');

        this.isValidSearch = true;
    });
  }

  showWeather(city: ICity): void {
    const { lat: lattitude, lon: longtitude, name: cityName } = city;

    this._darkSky.getWeather(lattitude, longtitude, cityName);
  }

  validation(): boolean {
    if (this.userSearchText.trim() !== '') { return; }

    this.citiesList = [];
  }

  ngOnDestroy() {
    if (this.citiesListSubscription) {
      this.citiesListSubscription.unsubscribe();
    }
  }
}
