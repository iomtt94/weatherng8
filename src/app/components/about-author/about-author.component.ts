import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-author',
  templateUrl: './about-author.component.html',
  styleUrls: ['./about-author.component.scss']
})

export class AboutAuthorComponent implements OnInit {

  authorName = 'Bohdan';

  constructor() { }

  ngOnInit() { }

}
