import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

  changeColor(color: string, event: MouseEvent) {
    event.preventDefault();
    document.documentElement.style.setProperty('--background', color.toString());
  }
}
