import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { TodoModule } from './todo-module/todo.module';
import { WeatherModule } from './weather-module/weather.module';
import { AboutAuthorComponent } from './components/about-author/about-author.component';
import { SharedModule } from './shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './interceptor/ErrorInterceptor';

const routes: Routes = [
  { path: '', redirectTo: 'todoList', pathMatch: 'full' },
  { path: 'author', component: AboutAuthorComponent },
  { path: '**', component: PageNotFoundComponent, pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    AboutAuthorComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    SharedModule,
    FormsModule,
    WeatherModule,
    TodoModule,
    RouterModule.forRoot(routes),
  ],
  exports: [],
  providers: [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpErrorInterceptor,
    multi: true
  }
],
  bootstrap: [AppComponent]
})

export class AppModule {}
