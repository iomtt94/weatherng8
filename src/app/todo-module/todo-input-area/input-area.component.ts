import { Component, OnInit } from '@angular/core';
import { TodoService } from '../services/todo.service';
import { ToDoItemModel } from '../models/todo-item-model';

@Component({
  selector: 'app-input-area',
  templateUrl: './input-area.component.html',
  styleUrls: ['./input-area.component.scss']
})

export class InputAreaComponent implements OnInit {
  protected userInputText = '';

  constructor(private _todoService: TodoService) { }

  ngOnInit() { }

  addTodo(userInput: string): ToDoItemModel {
    if (this.userInputText.trim() === '') { return; }
    this._todoService.addTodo(userInput);
    this.userInputText = '';
  }
}
