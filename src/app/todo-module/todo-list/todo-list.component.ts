import { Component, OnInit } from '@angular/core';
import { TodoService } from '../services/todo.service';
import { ToDoItemModel } from '../models/todo-item-model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})

export class TodoListComponent implements OnInit {
  todoList: ToDoItemModel[];

  constructor(private _todoService: TodoService) {}

  ngOnInit() {
    this.todoList = this._todoService.todosList;
  }
}
